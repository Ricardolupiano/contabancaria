package controller;

import java.util.ArrayList;
import modelConta.ContaCorrente;
import modelConta.ContaPoupanca;
import modelFuncionario.Funcionario;

public class ControleContas {
    
    private ArrayList<ContaCorrente> listaContaCorrente;
    private ArrayList<ContaPoupanca> listaContaPoupanca;

    public ControleContas() {
        listaContaCorrente = new ArrayList<ContaCorrente>();
        listaContaPoupanca = new ArrayList<ContaPoupanca>();
    }

    public ArrayList<ContaCorrente> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(ArrayList<ContaCorrente> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public ArrayList<ContaPoupanca> getListaContaPoupanca() {
        return listaContaPoupanca;
    }

    public void setListaContaPoupanca(ArrayList<ContaPoupanca> listaContaPoupanca) {
        this.listaContaPoupanca = listaContaPoupanca;
    }
    public void adicionarContaCorrente(ContaCorrente umaConta){
        listaContaCorrente.add(umaConta);
    }
    public void adicionarContaPoupanca(ContaPoupanca umConta){
        listaContaPoupanca.add(umConta);
    }
    public void excluirContaCorrente(ContaCorrente umaConta){
        listaContaCorrente.remove(umaConta);
    }
    public void excluirContaPoupanca(ContaPoupanca umaConta){
        listaContaPoupanca.remove(umaConta);
    }
    public ContaCorrente pesquisarContaCorrenteNumero(String numero){
        for(ContaCorrente umaContaCorrente : listaContaCorrente){
            if(umaContaCorrente.getNconta().equalsIgnoreCase(numero))
                return umaContaCorrente;
        }
        return null;
    }
    public ContaPoupanca pesquisarContaPoupancaNumero(String numero){
        for(ContaPoupanca umaContaPoupanca : listaContaPoupanca){
            if(umaContaPoupanca.getNconta().equalsIgnoreCase(numero))
                return umaContaPoupanca;
        }
        return null;
    }
    public ContaCorrente pesquisarContaCorrenteNome(String nomePesquisa){
        for(ContaCorrente umaContaCorrente : listaContaCorrente){
            if(umaContaCorrente.getCliente().getNome().equalsIgnoreCase(nomePesquisa))
                return umaContaCorrente;
        }
        return null;
    }
    
    public ContaPoupanca pesquisarContaPoupancaNome(String nomePesquisa){
        for(ContaPoupanca umaContaPoupanca : listaContaPoupanca){
            if(umaContaPoupanca.getCliente().getNome().equalsIgnoreCase(nomePesquisa))
                return umaContaPoupanca;
        }
        return null;
    }
    
}

