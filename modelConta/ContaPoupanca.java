package modelConta;


import modelCliente.PessoaFisica;
import modelCliente.PessoaJuridica;

public class ContaPoupanca extends Conta{
    
    private double rendimento;
    private String variacao;

    public ContaPoupanca(PessoaFisica cliente, double rendimento, String variacao,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        super(cliente,nconta, ncartao, agencia, limiteTransf,saldo);
        this.rendimento = rendimento;
        this.variacao = variacao;
    }
     public ContaPoupanca(PessoaJuridica cliente, double rendimento, String variacao,String nconta, String ncartao, String agencia, double limiteTransf,double saldo) {
        super(cliente,nconta, ncartao, agencia, limiteTransf,saldo);
        this.rendimento = rendimento;
        this.variacao = variacao;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public String getVariacao() {
        return variacao;
    }

    public void setVariacao(String variacao) {
        this.variacao = variacao;
    }
    
    
}
