package view;
import controller.ControleContas;
import controller.ControleFuncionario;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import modelCliente.Endereco;
import modelCliente.PessoaFisica;
import modelCliente.PessoaJuridica;
import modelConta.ContaCorrente;
import modelConta.ContaPoupanca;
import modelFuncionario.Funcionario;
import modelFuncionario.Gerente;

public class Tela extends javax.swing.JFrame {
    ControleContas umControleContas = new ControleContas();
    PessoaFisica Fisica;
    PessoaJuridica Juridica;
    ContaCorrente umaContaCorrente;
    ContaPoupanca umaContaPoupanca;
    Endereco umEndereco;
    Gerente umGerente;
    Funcionario umFuncionario;
    ControleFuncionario umControleFuncionario = new ControleFuncionario();
    boolean modoAlteracaoClienteFisico;
    boolean modoAlteracaoClienteJuridico;
    boolean modoAlteracaoEndereco;
    boolean modoAlteracaoCliente;
    boolean modoAlteracaoContaCorrente;
    boolean modoAlteracaoContaPoupanca;
    boolean modoAlteracaoConta;
    boolean modoAlteracaoFuncionario;
    boolean modoAlteracaoFuncionarioGerente;
    int tabela=1;
    boolean novaConta;
    boolean novoFuncionario;
    int tipoConta=0;
    int tipoFuncionario=1;
    TelaLogin umaTelaLogin;
    
    public Tela() {
        initComponents();
        modoAlteracaoClienteFisico= false;
        modoAlteracaoClienteJuridico=false;
        modoAlteracaoEndereco=false;
        modoAlteracaoCliente=false;
        habilitarDesabilitarCamposGeral();
        limparCamposGeral();
        jButton_Salvar.setEnabled(false);
        jButton_NovoClienteFisico.setEnabled(false);
        jButton_NovoClienteJuridico.setEnabled(false);
        jButton_NovaContaCorrente.setEnabled(true);
        jButton_NovaContaPoupanca.setEnabled(true);
        jButton_Editar.setEnabled(false);
        this.jTable_ListaContas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tabela=1;
        modoAlteracaoFuncionario=false;
        jButton_Excluir.setEnabled(false);
        jButton_SalvarFuncionario.setEnabled(false);
        jButton_EditarFuncionario.setEnabled(false);
        jButton_CancelarFuncionario.setEnabled(false);
        jButton_ExcluirFuncionario.setEnabled(false);
        umaTelaLogin= new TelaLogin();
        
    }
    private void exibirInformacao(String info){
        JOptionPane.showMessageDialog(this, info,"Atenção" ,JOptionPane.INFORMATION_MESSAGE);
    }
    public ControleFuncionario getUmControle(){
        return umControleFuncionario;
    }
    public void limparCamposCadastroFuncionario(){
        jTextField_NomeFuncionario.setText("");
        jTextField_TelefoneFuncionario.setText("");
        jTextField_MatriculaFuncionario.setText("");
        jPasswordField_SenhaFuncionario.setText("");
        jPasswordField_ConfirmarSenhaFuncionario.setText("");
        jTextField_NivelFuncionario.setText("1");
    }
    private void limparCamposConta(){
        jTextField_NumeroConta.setText("");
        jTextField_NumeroCartao.setText("");
        jTextField_LimiteTransferencia.setText("0.0");
        jTextField_Agencia.setText("");
        jTextField_Saldo.setText("0.0");
        jTextField_Rendimento.setText("0.0");
        jTextField_Variacao.setText("");
    }
    private void limparCamposCliente(){
        jTextField_NomeCliente.setText("");
        jTextField_Telefone1.setText("");
        jTextField_Telefone2.setText("");
        jTextField_Cpf.setText("");
        jTextField_Cnpj.setText("");
        jTextField_RazaoSocial.setText("");    
    }
    private void limparCamposEndereco(){
        jTextField_Bairro.setText("");
        jTextField_Cidade.setText("");
        jTextField_Complemento.setText("");
        jTextField_Estado.setText("");
        jTextField_Logradouro.setText("");
        jTextField_Pais.setText("");
        jTextField_Numero.setText("");
    }
    private void limparCamposGeral(){
        limparCamposCliente();
        limparCamposEndereco();
        limparCamposConta();
        limparCamposCadastroFuncionario();
    }
    private void preencherCamposCliente(ContaCorrente umaConta){
        PessoaFisica pFisica = umaConta.getUmaPessoaFisica();
        PessoaJuridica pJuridica = umaConta.getUmaPessoaJuridica();
        if(pJuridica!=null){
            jTextField_NomeCliente.setText(pJuridica.getNome());
            jTextField_Telefone1.setText(pJuridica.getTelefone1());
            jTextField_Telefone2.setText(pJuridica.getTelefone2());
            jTextField_Cnpj.setText(pJuridica.getCnpj());
            jTextField_RazaoSocial.setText(pJuridica.getRazaoSocial());
            
        }
        else if(pFisica!=null){  
            jTextField_NomeCliente.setText(pFisica.getNome());
            jTextField_Telefone1.setText(pFisica.getTelefone1());
            jTextField_Telefone2.setText(pFisica.getTelefone2());
            jTextField_Cpf.setText(pFisica.getCpf());
           
        }  
    }
    private void preencherCamposCliente(ContaPoupanca umaConta){
        if(umaConta.getUmaPessoaFisica() == null){
            PessoaJuridica pJuridica=umaConta.getUmaPessoaJuridica();
            jTextField_NomeCliente.setText(pJuridica.getNome());
            jTextField_Telefone1.setText(pJuridica.getTelefone1());
            jTextField_Telefone2.setText(pJuridica.getTelefone2());
            jTextField_Cnpj.setText(pJuridica.getCnpj());
            jTextField_RazaoSocial.setText(pJuridica.getRazaoSocial());
            
        }
        else if(umaConta.getUmaPessoaJuridica()==null){
            PessoaFisica pFisica=umaConta.getUmaPessoaFisica();   
            jTextField_NomeCliente.setText(pFisica.getNome());
            jTextField_Telefone1.setText(pFisica.getTelefone1());
            jTextField_Telefone2.setText(pFisica.getTelefone2());
            jTextField_Cpf.setText(pFisica.getCpf());
            
        }
    }
    
    private void preencherCampoEndereco(ContaCorrente umaConta){
        jTextField_Bairro.setText(umaConta.getCliente().getEndereco().getBairro());
        jTextField_Cidade.setText(umaConta.getCliente().getEndereco().getCidade());
        jTextField_Complemento.setText(umaConta.getCliente().getEndereco().getComplemento());
        jTextField_Estado.setText(umaConta.getCliente().getEndereco().getEstado());
        jTextField_Logradouro.setText(umaConta.getCliente().getEndereco().getLogradouro());
        jTextField_Pais.setText(umaConta.getCliente().getEndereco().getPais());
        jTextField_Numero.setText(umaConta.getCliente().getEndereco().getNumero());
    }
    private void preencherCampoEndereco(ContaPoupanca umaConta){
        jTextField_Bairro.setText(umaConta.getCliente().getEndereco().getBairro());
        jTextField_Cidade.setText(umaConta.getCliente().getEndereco().getCidade());
        jTextField_Complemento.setText(umaConta.getCliente().getEndereco().getComplemento());
        jTextField_Estado.setText(umaConta.getCliente().getEndereco().getEstado());
        jTextField_Logradouro.setText(umaConta.getCliente().getEndereco().getLogradouro());
        jTextField_Pais.setText(umaConta.getCliente().getEndereco().getPais());
        jTextField_Numero.setText(umaConta.getCliente().getEndereco().getNumero());
    }
    private void preencherCampoConta(ContaCorrente umaConta){
        jTextField_NumeroConta.setText(umaConta.getNconta());
        jTextField_NumeroCartao.setText(umaConta.getNcartao());
        jTextField_LimiteTransferencia.setText(Double.toString(umaConta.getLimiteTransf()));
        jTextField_Agencia.setText(umaConta.getAgencia());
        jTextField_Saldo.setText(Double.toString(umaConta.getSaldo()));        
    }
    private void preencherCampoConta(ContaPoupanca umaConta){
        jTextField_NumeroConta.setText(umaConta.getNconta());
        jTextField_NumeroCartao.setText(umaConta.getNcartao());
        jTextField_LimiteTransferencia.setText(Double.toString(umaConta.getLimiteTransf()));
        jTextField_Agencia.setText(umaConta.getAgencia());
        jTextField_Saldo.setText(Double.toString(umaConta.getSaldo()));
        jTextField_Rendimento.setText(Double.toString(umaConta.getRendimento()));
        jTextField_Variacao.setText(umaConta.getVariacao());
    }
    public void preencherCamposFuncionario(Funcionario umFuncionario){
        jTextField_NomeFuncionario.setText(umFuncionario.getNome());
        jTextField_TelefoneFuncionario.setText(umFuncionario.getTelefone());
        jTextField_MatriculaFuncionario.setText(umFuncionario.getMatricula());
        jPasswordField_SenhaFuncionario.setText(umFuncionario.getSenha());
        jPasswordField_ConfirmarSenhaFuncionario.setText(umFuncionario.getSenha());
        
    }
    public void preencherCamposFuncionario(Gerente umGerente){
        jTextField_NomeFuncionario.setText(umGerente.getNome());
        jTextField_TelefoneFuncionario.setText(umGerente.getTelefone());
        jTextField_MatriculaFuncionario.setText(umGerente.getMatricula());
        jPasswordField_SenhaFuncionario.setText(umGerente.getSenha());
        jPasswordField_ConfirmarSenhaFuncionario.setText(umGerente.getSenha());
        jTextField_NivelFuncionario.setText(Integer.toString(umGerente.getNivel()));
    }
    private void preencherCamposGeralConta(ContaCorrente umaConta){
        preencherCampoEndereco(umaConta);
        preencherCamposCliente(umaConta);
        preencherCampoConta(umaConta);
    }
    private void preencherCamposGeralConta(ContaPoupanca umaConta){
        preencherCampoEndereco(umaConta);
        preencherCamposCliente(umaConta);
        preencherCampoConta(umaConta);
    }
    private void habilitarDesabilitarCamposConta(){
        jTextField_NumeroConta.setEnabled(modoAlteracaoConta);
        jTextField_NumeroCartao.setEnabled(modoAlteracaoConta);
        jTextField_LimiteTransferencia.setEnabled(modoAlteracaoConta);
        jTextField_Agencia.setEnabled(modoAlteracaoConta);
        jTextField_LimiteTransferencia.setEnabled(modoAlteracaoConta);
        jTextField_Saldo.setEnabled(modoAlteracaoConta);
        jTextField_Rendimento.setEnabled(modoAlteracaoContaPoupanca);
        jTextField_Variacao.setEnabled(modoAlteracaoContaPoupanca);
    }
    private void habilitarDesabilitarCamposCliente(){
        jTextField_NomeCliente.setEnabled(modoAlteracaoCliente);
        jTextField_Telefone1.setEnabled(modoAlteracaoCliente);
        jTextField_Telefone2.setEnabled(modoAlteracaoCliente);
        jTextField_Cpf.setEnabled(modoAlteracaoClienteFisico);
        jTextField_Cnpj.setEnabled(modoAlteracaoClienteJuridico);
        jTextField_RazaoSocial.setEnabled(modoAlteracaoClienteJuridico);    
    }
    private void habilitarDesabilitarCamposEndereco(){
        jTextField_Bairro.setEnabled(modoAlteracaoEndereco);
        jTextField_Cidade.setEnabled(modoAlteracaoEndereco);
        jTextField_Complemento.setEnabled(modoAlteracaoEndereco);
        jTextField_Estado.setEnabled(modoAlteracaoEndereco);
        jTextField_Logradouro.setEnabled(modoAlteracaoEndereco);
        jTextField_Pais.setEnabled(modoAlteracaoEndereco);
        jTextField_Numero.setEnabled(modoAlteracaoEndereco);
    }
    public void habilitarDesabilitarCamposFuncionario(){
        jTextField_NomeFuncionario.setEnabled(modoAlteracaoFuncionario);
        jTextField_TelefoneFuncionario.setEnabled(modoAlteracaoFuncionario);
        jTextField_MatriculaFuncionario.setEnabled(modoAlteracaoFuncionario);
        jPasswordField_SenhaFuncionario.setEnabled(modoAlteracaoFuncionario);
        jPasswordField_ConfirmarSenhaFuncionario.setEnabled(modoAlteracaoFuncionario);
        jTextField_NivelFuncionario.setEnabled(modoAlteracaoFuncionarioGerente);
    }
    private void habilitarDesabilitarCamposGeral(){
        habilitarDesabilitarCamposCliente();
        habilitarDesabilitarCamposEndereco();
        habilitarDesabilitarCamposConta();
        habilitarDesabilitarCamposFuncionario();
    }
    private void carregarLista() {
        if(tabela==1){
            ArrayList<ContaCorrente> listaContaCorrente = umControleContas.getListaContaCorrente();
            DefaultTableModel model = (DefaultTableModel) jTable_ListaContas.getModel();
            model.setRowCount(0);
            for (ContaCorrente c : listaContaCorrente) {
                if(c.getUmaPessoaFisica()==null)
                    model.addRow(new String[]{c.getNconta(), c.getAgencia(),c.getCliente().getNome(),"Juridico"});
                else if(c.getUmaPessoaJuridica()==null){
                    model.addRow(new String[]{c.getNconta(), c.getAgencia(),c.getCliente().getNome(),"Fisico"});
                }
                
            }
           
            jTable_ListaContas.setModel(model);
        }
        else if(tabela==2)
        {
            ArrayList<ContaPoupanca> listaContaPoupanca = umControleContas.getListaContaPoupanca();
            DefaultTableModel model = (DefaultTableModel) jTable_ListaContas.getModel();
            model.setRowCount(0);
            for (ContaPoupanca c : listaContaPoupanca) {
                if(c.getUmaPessoaFisica()==null){
                    model.addRow(new String[]{c.getNconta(), c.getAgencia(),c.getCliente().getNome(),"Juridico"});
                }
                else if(c.getUmaPessoaJuridica()==null){
                    model.addRow(new String[]{c.getNconta(), c.getAgencia(),c.getCliente().getNome(),"Fisico"});
                }
                
            }
            jTable_ListaContas.setModel(model);
            
        }
    }
    public void carregarListaFuncionarios(){
        if(tipoFuncionario==1){
            ArrayList<Funcionario> listaFuncionarios = umControleFuncionario.getListaFuncionario();
            DefaultTableModel model = (DefaultTableModel) jTable_ListaFuncionarios.getModel();
            model.setRowCount(0);
            for (Funcionario f : listaFuncionarios) {
                model.addRow(new String[]{f.getNome(), f.getMatricula(),f.getTelefone()}); 
            }
           
            jTable_ListaFuncionarios.setModel(model);
        }
        else if(tipoFuncionario==2)
        {
            ArrayList<Gerente> listaGerentes = umControleFuncionario.getListaGerentes();
            DefaultTableModel model = (DefaultTableModel) jTable_ListaFuncionarios.getModel();
            model.setRowCount(0);
            for (Gerente g : listaGerentes) {
                model.addRow(new String[]{g.getNome(), g.getMatricula(),g.getTelefone(),Integer.toString(g.getNivel())});  
            }
            
            jTable_ListaFuncionarios.setModel(model);
            
        }
    }

   @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane_Geral = new javax.swing.JTabbedPane();
        jPanel_CadastroConta = new javax.swing.JPanel();
        jTabbedPane_Dados = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jTextField_NomeCliente = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jTextField_Telefone1 = new javax.swing.JTextField();
        jTextField_Telefone2 = new javax.swing.JTextField();
        jTextField_Cpf = new javax.swing.JTextField();
        jTextField_Cnpj = new javax.swing.JTextField();
        jTextField_RazaoSocial = new javax.swing.JTextField();
        jButton_NovoClienteFisico = new javax.swing.JButton();
        jButton_NovoClienteJuridico = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField_Logradouro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField_Numero = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField_Bairro = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextField_Cidade = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextField_Estado = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField_Pais = new javax.swing.JTextField();
        jTextField_Complemento = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jTextField_NumeroConta = new javax.swing.JTextField();
        jTextField_NumeroCartao = new javax.swing.JTextField();
        jTextField_Agencia = new javax.swing.JTextField();
        jTextField_LimiteTransferencia = new javax.swing.JTextField();
        jTextField_Saldo = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jTextField_Rendimento = new javax.swing.JTextField();
        jTextField_Variacao = new javax.swing.JTextField();
        jButton_NovaContaCorrente = new javax.swing.JButton();
        jButton_Salvar = new javax.swing.JButton();
        jComboBox_Tabela = new javax.swing.JComboBox();
        jButton_NovaContaPoupanca = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable_ListaContas = new javax.swing.JTable();
        jButton_Editar = new javax.swing.JButton();
        jButton_Excluir = new javax.swing.JButton();
        jPanel_CadastroFuncionario = new javax.swing.JPanel();
        jTextField_NomeFuncionario = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jTextField_TelefoneFuncionario = new javax.swing.JTextField();
        jTextField_MatriculaFuncionario = new javax.swing.JTextField();
        jTextField_NivelFuncionario = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable_ListaFuncionarios = new javax.swing.JTable();
        jButton_NovoFuncionario = new javax.swing.JButton();
        jButton_NovoGerente = new javax.swing.JButton();
        jPasswordField_SenhaFuncionario = new javax.swing.JPasswordField();
        jLabel26 = new javax.swing.JLabel();
        jPasswordField_ConfirmarSenhaFuncionario = new javax.swing.JPasswordField();
        jComboBox_TipoFuncionario = new javax.swing.JComboBox();
        jButton_SalvarFuncionario = new javax.swing.JButton();
        jButton_CancelarFuncionario = new javax.swing.JButton();
        jButton_EditarFuncionario = new javax.swing.JButton();
        jButton_ExcluirFuncionario = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel8.setText("Nome:");

        jLabel9.setText("Telefone 1:");

        jLabel10.setText("Telefone 2:");

        jLabel12.setText("Cpf:");

        jLabel13.setText("Cnpj:");

        jLabel14.setText("Razão Social:");

        jButton_NovoClienteFisico.setText("Novo Cliente Fisico");
        jButton_NovoClienteFisico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NovoClienteFisicoActionPerformed(evt);
            }
        });

        jButton_NovoClienteJuridico.setText("Novo Cliente Juridico");
        jButton_NovoClienteJuridico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NovoClienteJuridicoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField_NomeCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addComponent(jTextField_Telefone1, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addComponent(jTextField_Telefone2, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE)
                    .addComponent(jTextField_Cnpj)
                    .addComponent(jTextField_Cpf)
                    .addComponent(jTextField_RazaoSocial))
                .addGap(50, 50, 50)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton_NovoClienteJuridico, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE)
                    .addComponent(jButton_NovoClienteFisico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(218, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextField_NomeCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTextField_Telefone1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextField_Telefone2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_NovoClienteFisico))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextField_Cpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton_NovoClienteJuridico))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jTextField_Cnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jTextField_RazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        jTabbedPane_Dados.addTab("Dados Cliente", jPanel2);

        jLabel1.setText("Logradouro:");

        jLabel2.setText("Numero:");

        jLabel3.setText("Bairro:");

        jLabel4.setText("Cidade:");

        jLabel5.setText("Estado:");

        jLabel6.setText("País:");

        jLabel7.setText("Complemento:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jTextField_Complemento, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 391, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_Pais, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_Cidade, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_Bairro, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_Numero, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextField_Logradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField_Logradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField_Numero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextField_Bairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextField_Cidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jTextField_Estado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jTextField_Pais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField_Complemento)
                    .addComponent(jLabel7))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jTabbedPane_Dados.addTab("Endereço Cliente", jPanel3);

        jLabel11.setText("Numero da Conta:");

        jLabel15.setText("Numero do Cartão:");

        jLabel16.setText("Numero da agencia:");

        jLabel17.setText("Limite de Transferência:");

        jLabel18.setText("Saldo:");

        jLabel19.setText("Rendimento:");

        jLabel20.setText("Variação:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(jLabel20))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField_Rendimento, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_Saldo, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_LimiteTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_Agencia, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_NumeroCartao, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_NumeroConta, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_Variacao, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(342, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jTextField_NumeroConta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField_NumeroCartao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(jTextField_Agencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(jTextField_LimiteTransferencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(jTextField_Saldo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(jTextField_Rendimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(jTextField_Variacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jTabbedPane_Dados.addTab("Dados Conta", jPanel5);

        jButton_NovaContaCorrente.setText("Nova Conta Corrente");
        jButton_NovaContaCorrente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NovaContaCorrenteActionPerformed(evt);
            }
        });

        jButton_Salvar.setText("Salvar");
        jButton_Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SalvarActionPerformed(evt);
            }
        });

        jComboBox_Tabela.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Contas Correntes", "Contas Poupança", "" }));
        jComboBox_Tabela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_TabelaActionPerformed(evt);
            }
        });

        jButton_NovaContaPoupanca.setText("Nova Conta Poupança");
        jButton_NovaContaPoupanca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NovaContaPoupancaActionPerformed(evt);
            }
        });

        jTable_ListaContas.setModel(new javax.swing.table.DefaultTableModel
            (
                null,
                new String [] {
                    "Conta", "Agencia","Nome Cliente","Tipo Cliente"
                }
            )
            {
                @Override
                public boolean isCellEditable(int rowIndex, int mColIndex) {
                    return false;
                }
            });
            jTable_ListaContas.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTable_ListaContasMouseClicked(evt);
                }
            });
            jScrollPane2.setViewportView(jTable_ListaContas);

            jButton_Editar.setText("Editar");
            jButton_Editar.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton_EditarActionPerformed(evt);
                }
            });

            jButton_Excluir.setText("Excluir");
            jButton_Excluir.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton_ExcluirActionPerformed(evt);
                }
            });

            javax.swing.GroupLayout jPanel_CadastroContaLayout = new javax.swing.GroupLayout(jPanel_CadastroConta);
            jPanel_CadastroConta.setLayout(jPanel_CadastroContaLayout);
            jPanel_CadastroContaLayout.setHorizontalGroup(
                jPanel_CadastroContaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel_CadastroContaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel_CadastroContaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel_CadastroContaLayout.createSequentialGroup()
                            .addComponent(jButton_NovaContaCorrente)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton_NovaContaPoupanca)
                            .addGap(18, 18, 18)
                            .addComponent(jButton_Editar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton_Excluir)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jButton_Salvar)
                            .addGap(62, 62, 62))
                        .addComponent(jTabbedPane_Dados)
                        .addComponent(jScrollPane2))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox_Tabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12))
            );
            jPanel_CadastroContaLayout.setVerticalGroup(
                jPanel_CadastroContaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel_CadastroContaLayout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel_CadastroContaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton_NovaContaCorrente)
                        .addComponent(jButton_Salvar)
                        .addComponent(jButton_NovaContaPoupanca)
                        .addComponent(jButton_Editar)
                        .addComponent(jButton_Excluir))
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(jPanel_CadastroContaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel_CadastroContaLayout.createSequentialGroup()
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jTabbedPane_Dados, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jComboBox_Tabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );

            jTabbedPane_Geral.addTab("Cadastro Conta", jPanel_CadastroConta);

            jLabel21.setText("Nome Funcionario:");

            jLabel22.setText("Telefone:");

            jLabel23.setText("Matricula:");

            jLabel24.setText("Senha:");

            jLabel25.setText("Nível de autorização:");

            jTextField_NivelFuncionario.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jTextField_NivelFuncionarioActionPerformed(evt);
                }
            });

            jTable_ListaFuncionarios.setModel(new javax.swing.table.DefaultTableModel
                (
                    null,
                    new String [] {
                        "Nome", "Matricula","Telefone","Nivel"
                    }
                )
                {
                    @Override
                    public boolean isCellEditable(int rowIndex, int mColIndex) {
                        return false;
                    }
                });
                jTable_ListaFuncionarios.addMouseListener(new java.awt.event.MouseAdapter() {
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        jTable_ListaFuncionariosMouseClicked(evt);
                    }
                });
                jScrollPane1.setViewportView(jTable_ListaFuncionarios);

                jButton_NovoFuncionario.setText("Novo Funcionario");
                jButton_NovoFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_NovoFuncionarioActionPerformed(evt);
                    }
                });

                jButton_NovoGerente.setText("Novo Gerente");
                jButton_NovoGerente.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_NovoGerenteActionPerformed(evt);
                    }
                });

                jPasswordField_SenhaFuncionario.setText("jPasswordField1");

                jLabel26.setText("Confirmar Senha:");

                jPasswordField_ConfirmarSenhaFuncionario.setText("jPasswordField1");

                jComboBox_TipoFuncionario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Funcionário", "Gerente" }));
                jComboBox_TipoFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jComboBox_TipoFuncionarioActionPerformed(evt);
                    }
                });

                jButton_SalvarFuncionario.setText("Salvar");
                jButton_SalvarFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_SalvarFuncionarioActionPerformed(evt);
                    }
                });

                jButton_CancelarFuncionario.setText("Cancelar");
                jButton_CancelarFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_CancelarFuncionarioActionPerformed(evt);
                    }
                });

                jButton_EditarFuncionario.setText("Editar");
                jButton_EditarFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_EditarFuncionarioActionPerformed(evt);
                    }
                });

                jButton_ExcluirFuncionario.setText("Excluir");
                jButton_ExcluirFuncionario.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(java.awt.event.ActionEvent evt) {
                        jButton_ExcluirFuncionarioActionPerformed(evt);
                    }
                });

                javax.swing.GroupLayout jPanel_CadastroFuncionarioLayout = new javax.swing.GroupLayout(jPanel_CadastroFuncionario);
                jPanel_CadastroFuncionario.setLayout(jPanel_CadastroFuncionarioLayout);
                jPanel_CadastroFuncionarioLayout.setHorizontalGroup(
                    jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel22)
                                    .addComponent(jLabel23)
                                    .addComponent(jLabel24)
                                    .addComponent(jLabel26)
                                    .addComponent(jLabel25))
                                .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextField_NivelFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField_TelefoneFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextField_NomeFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jPasswordField_SenhaFuncionario)
                                                .addComponent(jTextField_MatriculaFuncionario, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                                .addComponent(jPasswordField_ConfirmarSenhaFuncionario, javax.swing.GroupLayout.Alignment.LEADING))))
                                    .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                                .addGap(23, 23, 23)
                                                .addComponent(jButton_NovoGerente)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton_EditarFuncionario)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jButton_ExcluirFuncionario)
                                                .addGap(29, 29, 29)
                                                .addComponent(jButton_SalvarFuncionario)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton_CancelarFuncionario))
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 588, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                .addGap(23, 23, 23)
                                .addComponent(jComboBox_TipoFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jButton_NovoFuncionario)))
                        .addContainerGap(187, Short.MAX_VALUE))
                );
                jPanel_CadastroFuncionarioLayout.setVerticalGroup(
                    jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel_CadastroFuncionarioLayout.createSequentialGroup()
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton_NovoFuncionario)
                            .addComponent(jButton_NovoGerente)
                            .addComponent(jButton_SalvarFuncionario)
                            .addComponent(jButton_CancelarFuncionario)
                            .addComponent(jButton_EditarFuncionario)
                            .addComponent(jButton_ExcluirFuncionario))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jComboBox_TipoFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField_NomeFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jTextField_TelefoneFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(jTextField_MatriculaFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel24)
                            .addComponent(jPasswordField_SenhaFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel26)
                            .addComponent(jPasswordField_ConfirmarSenhaFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel_CadastroFuncionarioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(jTextField_NivelFuncionario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(154, 154, 154))
                );

                jTabbedPane_Geral.addTab("Cadastro Funcionário", jPanel_CadastroFuncionario);

                javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
                getContentPane().setLayout(layout);
                layout.setHorizontalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane_Geral, javax.swing.GroupLayout.Alignment.TRAILING)
                );
                layout.setVerticalGroup(
                    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jTabbedPane_Geral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                );

                pack();
            }// </editor-fold>//GEN-END:initComponents

    private void jButton_NovaContaCorrenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovaContaCorrenteActionPerformed
       modoAlteracaoConta=true;
       modoAlteracaoCliente=true;
       modoAlteracaoEndereco=true;
       modoAlteracaoContaCorrente=true;
       modoAlteracaoContaPoupanca=false;
       jButton_NovaContaCorrente.setEnabled(false);
       jButton_NovaContaPoupanca.setEnabled(false);
       jButton_NovoClienteFisico.setEnabled(true);
       jButton_NovoClienteJuridico.setEnabled(true);
       novaConta=true;
       limparCamposGeral();
       
    }//GEN-LAST:event_jButton_NovaContaCorrenteActionPerformed

    private void jButton_SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SalvarActionPerformed
       String nome = jTextField_NomeCliente.getText();
       String telefone1=jTextField_Telefone1.getText();
       String telefone2=jTextField_Telefone2.getText();
       String cnpj = jTextField_Cnpj.getText();
       String razaoSocial = jTextField_RazaoSocial.getText();
       String cpf = jTextField_Cpf.getText();
       String logradouro = jTextField_Logradouro.getText();
       String numero = jTextField_Numero.getText();
       String bairro = jTextField_Bairro.getText();
       String cidade = jTextField_Cidade.getText();
       String estado = jTextField_Estado.getText();
       String pais = jTextField_Pais.getText();
       String complemento = jTextField_Complemento.getText();
      
       double limiteTransferencia = Double.parseDouble(jTextField_LimiteTransferencia.getText());
       double saldo = Double.parseDouble(jTextField_Saldo.getText());
       
            if(jTextField_NumeroConta.getText().equalsIgnoreCase("")){
                exibirInformacao("Digite um Numero para a conta!");
            }
            else{
                if(novaConta==false){
                    if(tipoConta==1){
                        if(umaContaPoupanca.getUmaPessoaFisica()==null){
                            umaContaPoupanca.getUmaPessoaJuridica().setNome(nome);
                            umaContaPoupanca.getUmaPessoaJuridica().setTelefone1(telefone1);
                            umaContaPoupanca.getUmaPessoaJuridica().setTelefone2(telefone2);
                            umaContaPoupanca.getUmaPessoaJuridica().setCnpj(cnpj);
                            umaContaPoupanca.getUmaPessoaJuridica().setRazaoSocial(razaoSocial);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setLogradouro(logradouro);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setNumero(numero);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setBairro(bairro);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setCidade(cidade);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setEstado(estado);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setPais(pais);
                            umaContaPoupanca.getUmaPessoaJuridica().getEndereco().setComplemento(complemento);
                            umaContaPoupanca.setLimiteTransf(limiteTransferencia);
                            umaContaPoupanca.setSaldo(saldo);
                        }
                        else if(umaContaPoupanca.getUmaPessoaJuridica()==null){
                            umaContaPoupanca.getUmaPessoaFisica().setNome(nome);
                            umaContaPoupanca.getUmaPessoaFisica().setTelefone1(telefone1);
                            umaContaPoupanca.getUmaPessoaFisica().setTelefone2(telefone2);
                            umaContaPoupanca.getUmaPessoaFisica().setCpf(cpf);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setLogradouro(logradouro);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setNumero(numero);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setBairro(bairro);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setCidade(cidade);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setEstado(estado);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setPais(pais);
                            umaContaPoupanca.getUmaPessoaFisica().getEndereco().setComplemento(complemento);
                            umaContaPoupanca.setLimiteTransf(limiteTransferencia);
                            umaContaPoupanca.setSaldo(saldo);
                        }

                    }
                    else if(tipoConta==2){
                        if(umaContaCorrente.getUmaPessoaFisica()==null){
                            umaContaCorrente.getUmaPessoaJuridica().setNome(nome);
                            umaContaCorrente.getUmaPessoaJuridica().setTelefone1(telefone1);
                            umaContaCorrente.getUmaPessoaJuridica().setTelefone2(telefone2);
                            umaContaCorrente.getUmaPessoaJuridica().setCnpj(cnpj);
                            umaContaCorrente.getUmaPessoaJuridica().setRazaoSocial(razaoSocial);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setLogradouro(logradouro);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setNumero(numero);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setBairro(bairro);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setCidade(cidade);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setEstado(estado);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setPais(pais);
                            umaContaCorrente.getUmaPessoaJuridica().getEndereco().setComplemento(complemento);
                            umaContaCorrente.setLimiteTransf(limiteTransferencia);
                            umaContaCorrente.setSaldo(saldo);
                        }
                        else if(umaContaCorrente.getUmaPessoaJuridica()==null){
                            umaContaCorrente.getUmaPessoaFisica().setNome(nome);
                            umaContaCorrente.getUmaPessoaFisica().setTelefone1(telefone1);
                            umaContaCorrente.getUmaPessoaFisica().setTelefone2(telefone2);
                            umaContaCorrente.getUmaPessoaFisica().setCpf(cpf);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setLogradouro(logradouro);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setNumero(numero);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setBairro(bairro);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setCidade(cidade);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setEstado(estado);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setPais(pais);
                            umaContaCorrente.getUmaPessoaFisica().getEndereco().setComplemento(complemento);
                            umaContaCorrente.setLimiteTransf(limiteTransferencia);
                            umaContaCorrente.setSaldo(saldo);
                        }

                    }
                }
                
                else if(novaConta=true){
                    umEndereco = new Endereco(logradouro, numero, bairro, cidade, estado, pais, complemento);
                    String numeroConta = jTextField_NumeroConta.getText();
                    String numeroCartao = jTextField_NumeroCartao.getText();
                    String numeroAgencia= jTextField_Agencia.getText();
            
                    if(modoAlteracaoContaCorrente==true){
                        if(modoAlteracaoClienteFisico==true){
                            Fisica = new PessoaFisica(cpf, nome, telefone1, telefone2, umEndereco);
                            umaContaCorrente = new ContaCorrente(Fisica, numeroConta, numeroCartao, numeroAgencia, limiteTransferencia, saldo);
                            umControleContas.adicionarContaCorrente(umaContaCorrente);
                        }
                        else if(modoAlteracaoClienteJuridico==true){
                            Juridica = new PessoaJuridica(nome, telefone1, telefone2, umEndereco, cnpj, razaoSocial);
                            umaContaCorrente = new ContaCorrente(Juridica, numeroConta, numeroCartao, numeroAgencia, limiteTransferencia, saldo);
                            umControleContas.adicionarContaCorrente(umaContaCorrente);
                        }
                    }
                    else if(modoAlteracaoContaPoupanca==true){
                        double rendimento = Double.parseDouble(jTextField_Rendimento.getText());
                        String variacao = jTextField_Variacao.getText();
                        if(modoAlteracaoClienteFisico==true){
                            Fisica = new PessoaFisica(cpf, nome, telefone1, telefone2, umEndereco);
                            umaContaPoupanca = new ContaPoupanca(Fisica, rendimento, variacao, numeroConta, numeroCartao, numeroAgencia, limiteTransferencia, saldo);
                            umControleContas.adicionarContaPoupanca(umaContaPoupanca);
                        }
                        else if(modoAlteracaoClienteJuridico==true){
                            Juridica = new PessoaJuridica(nome, telefone1, telefone2, umEndereco, cnpj, razaoSocial);
                            umaContaPoupanca = new ContaPoupanca(Juridica, rendimento, variacao, numeroConta, numeroCartao, numeroAgencia, limiteTransferencia, saldo);
                            umControleContas.adicionarContaPoupanca(umaContaPoupanca);
                        }
                    }
                }
            jButton_Salvar.setEnabled(false);
            jButton_NovaContaCorrente.setEnabled(true);
            jButton_NovaContaPoupanca.setEnabled(true);
            limparCamposGeral();
            modoAlteracaoCliente=false;
            modoAlteracaoClienteFisico=false;
            modoAlteracaoClienteJuridico=false;
            modoAlteracaoConta=false;
            modoAlteracaoContaPoupanca=false;
            modoAlteracaoEndereco=false;
            habilitarDesabilitarCamposGeral();
            carregarLista();
            }

    }//GEN-LAST:event_jButton_SalvarActionPerformed

    private void jComboBox_TabelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_TabelaActionPerformed
       switch (jComboBox_Tabela.getSelectedItem().toString()) {
            case "Contas Correntes":
                tabela=1;
                carregarLista();
                break;
            case "Contas Poupança":
                tabela=2;
                carregarLista();
                break;
        }
    }//GEN-LAST:event_jComboBox_TabelaActionPerformed

    private void jButton_NovaContaPoupancaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovaContaPoupancaActionPerformed
       modoAlteracaoConta=true;
       modoAlteracaoCliente=true;
       modoAlteracaoEndereco=true;
       modoAlteracaoContaCorrente=false;
       modoAlteracaoContaPoupanca=true;
       jButton_NovaContaCorrente.setEnabled(false);
       jButton_NovaContaPoupanca.setEnabled(false);
       jButton_NovoClienteFisico.setEnabled(true);
       jButton_NovoClienteJuridico.setEnabled(true);
       novaConta=true;
       limparCamposGeral();
    }//GEN-LAST:event_jButton_NovaContaPoupancaActionPerformed

    private void jButton_NovoClienteJuridicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovoClienteJuridicoActionPerformed
        modoAlteracaoClienteFisico=false;
       modoAlteracaoClienteJuridico=true;
       habilitarDesabilitarCamposGeral();
       jButton_Salvar.setEnabled(true);
       jButton_NovoClienteFisico.setEnabled(false);
       jButton_NovoClienteJuridico.setEnabled(false);
       jTextField_NomeCliente.requestFocus();
    }//GEN-LAST:event_jButton_NovoClienteJuridicoActionPerformed

    private void jButton_NovoClienteFisicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovoClienteFisicoActionPerformed
       modoAlteracaoClienteFisico=true;
       modoAlteracaoClienteJuridico=false;
       habilitarDesabilitarCamposGeral();
       jButton_Salvar.setEnabled(true);
       jButton_NovoClienteFisico.setEnabled(false);
       jButton_NovoClienteJuridico.setEnabled(false);
       jTextField_NomeCliente.requestFocus();
    }//GEN-LAST:event_jButton_NovoClienteFisicoActionPerformed

    private void jTable_ListaContasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_ListaContasMouseClicked
       DefaultTableModel model = (DefaultTableModel) jTable_ListaContas.getModel();
        String Numero = (String) model.getValueAt(jTable_ListaContas.getSelectedRow(), 0);
        umaContaCorrente = umControleContas.pesquisarContaCorrenteNumero(Numero);
        umaContaPoupanca = umControleContas.pesquisarContaPoupancaNumero(Numero);
        
        if(umaContaCorrente==null){
            preencherCamposGeralConta(umaContaPoupanca);
            tipoConta=1;
        }
        else if(umaContaPoupanca==null){
            preencherCamposGeralConta(umaContaCorrente);
            tipoConta=2;
        }
        
        
        jButton_Editar.setEnabled(true);
        jButton_Excluir.setEnabled(true);
    }//GEN-LAST:event_jTable_ListaContasMouseClicked

    private void jButton_EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_EditarActionPerformed

        if(tipoConta==1){
           if(umaContaPoupanca.getUmaPessoaFisica()==null){
               modoAlteracaoClienteJuridico=true;
               modoAlteracaoCliente=true;
               modoAlteracaoClienteFisico=false;
               modoAlteracaoConta=true;
               modoAlteracaoContaPoupanca=true;
               modoAlteracaoContaCorrente=false;
               modoAlteracaoEndereco=true;
               habilitarDesabilitarCamposGeral();
           }
           else if (umaContaPoupanca.getUmaPessoaJuridica()==null){
               modoAlteracaoClienteJuridico=false;
               modoAlteracaoCliente=true;
               modoAlteracaoClienteFisico=true;
               modoAlteracaoConta=true;
               modoAlteracaoContaPoupanca=true;
               modoAlteracaoContaCorrente=false;
               modoAlteracaoEndereco=true;
               habilitarDesabilitarCamposGeral();
           }
       }
       else if(tipoConta==2){
           if(umaContaCorrente.getUmaPessoaFisica()==null){
               modoAlteracaoClienteJuridico=true;
               modoAlteracaoCliente=true;
               modoAlteracaoClienteFisico=false;
               modoAlteracaoConta=true;
               modoAlteracaoContaPoupanca=false;
               modoAlteracaoContaCorrente=true;
               modoAlteracaoEndereco=true;
               habilitarDesabilitarCamposGeral();
           }
           else if(umaContaCorrente.getUmaPessoaJuridica()==null){
               modoAlteracaoClienteJuridico=false;
               modoAlteracaoCliente=true;
               modoAlteracaoClienteFisico=true;
               modoAlteracaoConta=true;
               modoAlteracaoContaPoupanca=false;
               modoAlteracaoContaCorrente=true;
               modoAlteracaoEndereco=true;
               habilitarDesabilitarCamposGeral();
           }
       }
       novaConta=false;
       jButton_Salvar.setEnabled(true);
    }//GEN-LAST:event_jButton_EditarActionPerformed

    private void jButton_ExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExcluirActionPerformed
       if(umaContaCorrente==null){
            umControleContas.excluirContaPoupanca(umaContaPoupanca);
        }
        else if(umaContaPoupanca==null){
            umControleContas.excluirContaCorrente(umaContaCorrente);
        }
       carregarLista();
       limparCamposGeral();
    }//GEN-LAST:event_jButton_ExcluirActionPerformed

    private void jTextField_NivelFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_NivelFuncionarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextField_NivelFuncionarioActionPerformed

    private void jButton_NovoFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovoFuncionarioActionPerformed
        modoAlteracaoFuncionario=true;
        modoAlteracaoFuncionarioGerente=false;
        habilitarDesabilitarCamposFuncionario();
        novoFuncionario=true;
        jButton_SalvarFuncionario.setEnabled(true);
        jButton_NovoFuncionario.setEnabled(false);
        jButton_NovoGerente.setEnabled(false);
        jButton_CancelarFuncionario.setEnabled(true);
    }//GEN-LAST:event_jButton_NovoFuncionarioActionPerformed

    private void jButton_NovoGerenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NovoGerenteActionPerformed
        modoAlteracaoFuncionario=true;
        modoAlteracaoFuncionarioGerente=true;
        habilitarDesabilitarCamposFuncionario();
        novoFuncionario=true;
        jButton_SalvarFuncionario.setEnabled(true);
        jButton_NovoFuncionario.setEnabled(false);
        jButton_NovoGerente.setEnabled(false);
        jButton_CancelarFuncionario.setEnabled(true);
    }//GEN-LAST:event_jButton_NovoGerenteActionPerformed

    private void jButton_SalvarFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SalvarFuncionarioActionPerformed
        String nome = jTextField_NomeFuncionario.getText();
        String telefone = jTextField_TelefoneFuncionario.getText();
        String matricula = jTextField_MatriculaFuncionario.getText();
        String senha1 = new String (jPasswordField_SenhaFuncionario.getPassword());
        String senha2 = new String (jPasswordField_ConfirmarSenhaFuncionario.getPassword());
        int nivel = Integer.parseInt(jTextField_NivelFuncionario.getText());
        
        if(nome.equalsIgnoreCase("")){
            exibirInformacao("Digite um Nome para o Funcionário");
        }
        else if(matricula.equalsIgnoreCase(""))
            exibirInformacao("Informe uma matrícula!");
        
        else if(!senha1.equals(senha2)){
            exibirInformacao("Senhas nao conferem!");
        }
        
        else{
            if(novoFuncionario==false){
                if(modoAlteracaoFuncionarioGerente==true){
                    umGerente.setNome(nome);
                    umGerente.setMatricula(matricula);
                    umGerente.setTelefone(telefone);
                    umGerente.setSenha(senha1);
                    umGerente.setNivel(nivel);
                }
                else if(modoAlteracaoFuncionarioGerente==false){
                    umFuncionario.setNome(nome);
                    umFuncionario.setMatricula(matricula);
                    umFuncionario.setTelefone(telefone);
                    umFuncionario.setSenha(senha1);
                }
            }
            else if(novoFuncionario==true){
                if(modoAlteracaoFuncionarioGerente==true){
                    umGerente = new Gerente(nivel, nome, telefone, matricula, senha1);
                    umControleFuncionario.adicionarGerente(umGerente);
                }
                else if(modoAlteracaoFuncionarioGerente==false){
                    umFuncionario = new Funcionario(nome, telefone, matricula, senha1);
                    umControleFuncionario.adicionarFuncionario(umFuncionario);
                }
            }
        }
        modoAlteracaoFuncionario=false;
        modoAlteracaoFuncionarioGerente=false;
        habilitarDesabilitarCamposFuncionario();
        carregarListaFuncionarios();
        limparCamposCadastroFuncionario();
        jButton_SalvarFuncionario.setEnabled(false);
        jButton_NovoFuncionario.setEnabled(true);
        jButton_NovoGerente.setEnabled(true);
        jButton_CancelarFuncionario.setEnabled(false);
        jButton_EditarFuncionario.setEnabled(false);
        jButton_ExcluirFuncionario.setEnabled(false);
       // umaTelaLogin.setAlwaysOnTop(true);
        
    }//GEN-LAST:event_jButton_SalvarFuncionarioActionPerformed

    private void jComboBox_TipoFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_TipoFuncionarioActionPerformed
        switch (jComboBox_TipoFuncionario.getSelectedItem().toString()) {
            case "Funcionário":
                tipoFuncionario=1;
                carregarListaFuncionarios();
                break;
            case "Gerente":
                tipoFuncionario=2;
                carregarListaFuncionarios();
                break;
        }
    }//GEN-LAST:event_jComboBox_TipoFuncionarioActionPerformed

    private void jButton_CancelarFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_CancelarFuncionarioActionPerformed
        jButton_NovoFuncionario.setEnabled(true);
        jButton_NovoGerente.setEnabled(true);
        jButton_CancelarFuncionario.setEnabled(false);
        jButton_SalvarFuncionario.setEnabled(false);
        modoAlteracaoFuncionario=false;
        modoAlteracaoFuncionarioGerente=false;
        habilitarDesabilitarCamposFuncionario();
        limparCamposCadastroFuncionario();
    }//GEN-LAST:event_jButton_CancelarFuncionarioActionPerformed

    private void jButton_EditarFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_EditarFuncionarioActionPerformed
        modoAlteracaoFuncionario=true;
        if(tipoFuncionario==2){
            modoAlteracaoFuncionarioGerente=true;
        }
        habilitarDesabilitarCamposFuncionario();
        jButton_SalvarFuncionario.setEnabled(true);
        jButton_NovoFuncionario.setEnabled(false);
        jButton_NovoGerente.setEnabled(false);
        jButton_EditarFuncionario.setEnabled(false);
        jButton_CancelarFuncionario.setEnabled(true);
        novoFuncionario=false;
    }//GEN-LAST:event_jButton_EditarFuncionarioActionPerformed

    private void jTable_ListaFuncionariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable_ListaFuncionariosMouseClicked
        DefaultTableModel model = (DefaultTableModel) jTable_ListaFuncionarios.getModel();
        String matricula = (String) model.getValueAt(jTable_ListaFuncionarios.getSelectedRow(),1);
        if(tipoFuncionario==1){
            umFuncionario=umControleFuncionario.pesquisarFuncionarioMatricula(matricula);
            preencherCamposFuncionario(umFuncionario);
        }
        else if(tipoFuncionario==2){
            umGerente=umControleFuncionario.pesquisarGerenteMatricula(matricula);
            preencherCamposFuncionario(umGerente);
        }
        jButton_EditarFuncionario.setEnabled(true);
        jButton_ExcluirFuncionario.setEnabled(true);
    }//GEN-LAST:event_jTable_ListaFuncionariosMouseClicked

    private void jButton_ExcluirFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExcluirFuncionarioActionPerformed
        if(tipoFuncionario==1){
            umControleFuncionario.excluirFuncionario(umFuncionario);
        }
        else if(tipoFuncionario==2){
            umControleFuncionario.excluirGerente(umGerente);
        }
        carregarListaFuncionarios();
        jButton_ExcluirFuncionario.setEnabled(false);
        jButton_CancelarFuncionario.setEnabled(false);
        jButton_SalvarFuncionario.setEnabled(false);
        jButton_NovoFuncionario.setEnabled(true);
        jButton_NovoGerente.setEnabled(true);
        limparCamposCadastroFuncionario();
    }//GEN-LAST:event_jButton_ExcluirFuncionarioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tela.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tela().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_CancelarFuncionario;
    private javax.swing.JButton jButton_Editar;
    private javax.swing.JButton jButton_EditarFuncionario;
    private javax.swing.JButton jButton_Excluir;
    private javax.swing.JButton jButton_ExcluirFuncionario;
    private javax.swing.JButton jButton_NovaContaCorrente;
    private javax.swing.JButton jButton_NovaContaPoupanca;
    private javax.swing.JButton jButton_NovoClienteFisico;
    private javax.swing.JButton jButton_NovoClienteJuridico;
    private javax.swing.JButton jButton_NovoFuncionario;
    private javax.swing.JButton jButton_NovoGerente;
    private javax.swing.JButton jButton_Salvar;
    private javax.swing.JButton jButton_SalvarFuncionario;
    private javax.swing.JComboBox jComboBox_Tabela;
    private javax.swing.JComboBox jComboBox_TipoFuncionario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel_CadastroConta;
    private javax.swing.JPanel jPanel_CadastroFuncionario;
    private javax.swing.JPasswordField jPasswordField_ConfirmarSenhaFuncionario;
    private javax.swing.JPasswordField jPasswordField_SenhaFuncionario;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane_Dados;
    private javax.swing.JTabbedPane jTabbedPane_Geral;
    private javax.swing.JTable jTable_ListaContas;
    private javax.swing.JTable jTable_ListaFuncionarios;
    private javax.swing.JTextField jTextField_Agencia;
    private javax.swing.JTextField jTextField_Bairro;
    private javax.swing.JTextField jTextField_Cidade;
    private javax.swing.JTextField jTextField_Cnpj;
    private javax.swing.JTextField jTextField_Complemento;
    private javax.swing.JTextField jTextField_Cpf;
    private javax.swing.JTextField jTextField_Estado;
    private javax.swing.JTextField jTextField_LimiteTransferencia;
    private javax.swing.JTextField jTextField_Logradouro;
    private javax.swing.JTextField jTextField_MatriculaFuncionario;
    private javax.swing.JTextField jTextField_NivelFuncionario;
    private javax.swing.JTextField jTextField_NomeCliente;
    private javax.swing.JTextField jTextField_NomeFuncionario;
    private javax.swing.JTextField jTextField_Numero;
    private javax.swing.JTextField jTextField_NumeroCartao;
    private javax.swing.JTextField jTextField_NumeroConta;
    private javax.swing.JTextField jTextField_Pais;
    private javax.swing.JTextField jTextField_RazaoSocial;
    private javax.swing.JTextField jTextField_Rendimento;
    private javax.swing.JTextField jTextField_Saldo;
    private javax.swing.JTextField jTextField_Telefone1;
    private javax.swing.JTextField jTextField_Telefone2;
    private javax.swing.JTextField jTextField_TelefoneFuncionario;
    private javax.swing.JTextField jTextField_Variacao;
    // End of variables declaration//GEN-END:variables
}
