package modelFuncionario;

public class Gerente extends Funcionario{
    
    private int nivel;

    public Gerente(int nivel, String nome, String telefone, String matricula,String senha) {
        super(nome, telefone, matricula,senha);
        this.nivel = nivel;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    
    
}
